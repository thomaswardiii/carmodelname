<?php
require 'vendor/autoload.php';

/*
 **** Load .env config file ****
 */
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

/*
 **** Slim containers ****
 */
$container = new \Slim\Container;

// Register component on container
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(getenv('TEMPLATE_PATH'), [
        'cache' => getenv('TEMPLATE_CACHE_PATH')
    ]);
    $view->addExtension(new \Slim\Views\TwigExtension(
        $c['router'],
        $c['request']->getUri()
    ));

    return $view;
};

/*
 **** Initialize Slim ****
 */
$app = new \Slim\App($container);

/*
 **** Middleware ****
 */
// Maintenance mode middleware. Gets used when $_ENV['SLIM_MODE'] == 'maintenance'
$app->add(new \Twardiii\Middleware\Maintenance(
    function ($request, &$response) use ($app)
    {
        $response = $response->withStatus(503);
        if (!request_wants_json($request) && !request_wants_xml($request))
        {
            $app->view->render($response, 'maintenance.html.twig');
        }
        return $response;
    }
));

/*
 **** Routes ****
 */
// Main page for displaying form
$app->get('/', function ($request, $response, $args) {
    if (request_wants_json($request) || request_wants_xml($request))
    {
        return $response->withStatus(406);
    }
    else
    {
        $this->view->render($response, 'form.html.twig');
    }
})->setName('root');

// API help
$app->get('/api', function ($request, $response, $args) {
    if (request_wants_json($request) || request_wants_xml($request))
    {
        return $response->withStatus(406);
    }
    else
    {
        $this->view->render($response, 'api.html.twig');
    }
})->setName('api');

// Process form
$app->get('/car-model-score', function($request, $response, $args) {
    $data = $request->getQueryParams();
    $name = $data['model_name'];

    $character_score = array(
        '0' => 60,
        '1' => -74,
        '2' => 6,
        '3' => 55,
        '4' => 35,
        '5' => 74,
        '6' => 6,
        '7' => -58,
        '8' => -67,
        '9' => -37,
        'a' => -14,
        'b' => -5,
        'c' => 27,
        'd' => -21,
        'e' => -45,
        'f' => 5,
        'g' => 27,
        'h' => -44,
        'i' => -21,
        'j' => 64,
        'k' => 32,
        'l' => 12,
        'm' => 19,
        'n' => -46,
        'o' => -80,
        'p' => -27,
        'q' => 40,
        'r' => 8,
        's' => 15,
        't' => -18,
        'u' => -68,
        'v' => 41,
        'w' => -20,
        'x' => 126,
        'y' => -90,
        'z' => 83,
    );

    $total_score = 0;
    $average_character_score = 0;
    $name_length = strlen($name);
    if ($name_length > 0)
    {
        $name_character_array = str_split(strtolower($name));
        foreach ($name_character_array as $character)
        {
            if (isset($character_score[$character]))
            {
                $total_score += $character_score[$character];
            }
        }

        $average_character_score = round($total_score / $name_length / 10, 2);
    }

    $response_data = array('model_name' => htmlentities($name), 'score' => $average_character_score);
    if (request_wants_json($request))
    {
        $response->getBody()->write(json_encode($response_data));
    }
    else if (request_wants_xml($request))
    {
        $xml_data = new SimpleXMLElement('<?xml version="1.0"?><response></response>');
        array_to_xml($response_data, $xml_data);
        $response->getBody()->write($xml_data->asXML());
    }
    else
    {
        $this->view->render($response, 'result.html.twig', $response_data);
    }
})->setName('car_model_score');

/*
 **** Go go go! ****
 */
$app->run();

function request_wants_json($request)
{
    $accepted_data_array = $request->getHeader('Accept');
    foreach ($accepted_data_array as $accepted_data)
    {
        if ($accepted_data === 'application/json')
        {
            return true;
        }
    }

    return false;
}

function request_wants_xml($request)
{
    $accepted_data_array = $request->getHeader('Accept');
    foreach ($accepted_data_array as $accepted_data)
    {
        if ($accepted_data === 'application/xml')
        {
            return true;
        }
    }

    return false;
}

function array_to_xml($data, &$xml_data)
{
    foreach ($data as $key => $value)
    {
        if (is_array($value))
        {
            // Prefix numeric-only keys with "item" for XML
            if (is_numeric($key))
            {
                $key = 'item' . $key;
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        }
        else
        {
            $xml_data->addChild("$key", htmlspecialchars("$value"));
        }
     }
}